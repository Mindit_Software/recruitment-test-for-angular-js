## Expected

- Contentful data model
    - include 'section' in 'article' as reference
    - assign correct types to all fields (e.g. title, slug, synopsis are 255 char fields, text is unbounded)
- Navigation
    - use some form of Angular routing (e.g. `ngRoute`), supporting 'back' browser action
    - form correct URLs (e.g. /mySection/myArticle)
    - populate the page header correctly (title, metas), making sure to remove previous metas
- API usage
    - API parameters (space, credentials) should not be hardcoded
    - The section 'blog' should be paginated, i.e. should load only the first N articles, then, with a button, or on scroll, should load 'older' articles
    - The section 'blog' should load a projection of the full content, excluding the text