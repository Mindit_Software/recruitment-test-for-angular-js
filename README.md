# Test AngularJS

## Create Contentful account

Create the following data model:


- Section:
    - section name
    - slug
    - meta:description
    - meta:keywords
- Article:
    - title
    - slug
    - author
    - date
    - text
    - synopsis
    - image
    - section
    - meta:description
    - meta:keywords

Use the [Contentful API](https://www.contentful.com/developers/docs/references/content-delivery-api/) to work with the content created (lorem ipsum).

## Create an AngularJS application with the following requirements:

### Functional requirements

- Display a navigation bar with the section names from the Contentful account
    - The landing page should point to the first section
    - When a section is accesed:
        - The URL should change to the section slug
        - The page title should change to the section name
        - The head of the page should include the section meta tags
- Each page accessed through navigation should load from Contentful a list of articles belonging to the respective section, in descending order of date
    - The page should display the title, author, date, image and synopsis of each article (format appropriately)
    - Each article box should have a "Read more" link that should expand it with the full text
    - When expanded:
        - The URL should change to include the section slug and article slug
        - the page title should change to the article title
        - the head of the page should include the article meta tags
- On the navigation bar there is also a search field
    - The search should be performed as the user types, but not for less than 4 characters
    - The search should be performed on the title and author fields only
    - The search should be performed only within the currently selected section

### Non-functional requirements

- Consider the following performance information when writing the application:
    - the number of articles in each section may be very large (tens of thousands)
    - the text content of each article may be very large (hundreds of paragraphs), while the synopsis will always have at most 255 characters.

## Expected

- Test duration: max 4h
- Test assessment guidelines: [See Expectatons](EXPECTED.md)
